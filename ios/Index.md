<!-- # VeriFlix iOS SDK documentation -->

<!-- Use https://github.com/jonschlinkert/markdown-toc to generate TOC below -->

<!-- toc -->

  * [Installation](#installation)
    + [CocoaPods](#cocoapods)
    + [Manual installation](#manual-installation)
  * [Sample app](#sample-app)
  * [Using VeriFlix SDK services](#using-veriflix-sdk-services)
  * [SDK Services overview](#sdk-services-overview)
  * [UI components](#ui-components)
  * [Suggested workflow of using SDK](#suggested-workflow-of-using-sdk)
    + [Configuration](#configuration)
    + [Check SDK availability](#check-sdk-availability)
    + [Authentication](#authentication)
    + [Update information about current device geolocation](#update-information-about-current-device-geolocation)
    + [Video recording and sending](#video-recording-and-sending)
      - [Example](#example)
      - [Additional configuration](#additional-configuration)
    + [Retrying upload](#retrying-upload)
    + [Recorded videos](#recorded-videos)
    + [User Profile](#user-profile)
      - [Getting a user profile information](#getting-a-user-profile-information)
      - [Updating a user profile information](#updating-a-user-profile-information)
  * [Theme support](#theme-support)
    + [SI Theme](#si-theme)
  * [Error types](#error-types)
- [Changelog](#changelog)
  * [[1.2.0] - 2021-11-03](#120---2021-11-03)
    + [Added](#added)
    + [Changes](#changes)
    + [Fixed](#fixed)
  * [[1.3.0] - 2021-26-03](#130---2021-26-03)
    + [Added](#added-1)
    + [Changes](#changes-1)
    + [Fixed](#fixed-1)
    + [[1.6.0] - 2022-20-09](#160---2022-20-09)
    + [Added](#added-2)

<!-- tocstop -->

## Installation

SDK requires iOS version 11 or greater.

### CocoaPods
If you are using CocoaPods, add the following to your `Podfile`:

``` {.ruby}
pod 'VeriflixSDK', :git => 'https://bitbucket.org/veriflix/veriflix-ios-sdk-bin.git'
```
Optionally specify a needed tag:
``` {.ruby}
pod 'VeriflixSDK', :git => 'https://bitbucket.org/veriflix/veriflix-ios-sdk-bin.git', :tag => '1.6.0'
```

Also you need to add following lines to your `Podfile`:
``` {.ruby}
post_install do |installer|
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
        end
    end
end
```

When updating to new version of SDK, run `pod update`.

### Manual installation
If you don't use Cocoapods, download `VeriflixSDK.xcframework` file from [https://bitbucket.org/veriflix/veriflix-ios-sdk-bin/src/master/](https://bitbucket.org/veriflix/veriflix-ios-sdk-bin/src/master/) and add it to Xcode.

## Sample app

You can download a sample app which uses SDK [here](https://bitbucket.org/veriflix/veriflix-ios-sdk-sampleapp/src/master/). 

## Using VeriFlix SDK services

All public services available through `Veriflix.sdk` static instance field and might be used after initial configuration of Veriflix SDK. All service methods accept callback function with `Result` argument. Result could be `.success` with specific type of data or `.failed` with specific type of error.

## SDK Services overview
VeriFlix SDK allows developers to use the VeriFlix Backend API  through the following services:

<!-- * `ActivityService` contains methods related to user activity:
    * Request a paginated list of user activity items for the current user
    * Request a count of new user activity items
    * Mark new user activity items as outdated -->

* `AuthService` contains methods for user authentication:
    * Log In
    * Sign Up
    * Password reset
    * Logout
    * Check username availability during the user registration process
    * Check is user authenticated
    <!-- * Check EULA accept status
    * Accept EULA -->

* `AvailabilityService` contains methods related to SDK availability.

* `PermissionsService` contains methods which may be used to simplify working with device permissions:
    * Get current permissions state
    * Request authorization to use specific permissions
 
* `PhotoLibrarySaver` is utility service which can be used to save file from local filesystem to Photo Library.

* `ProfileService` contains methods to work with user profile information:
    * Request user profile information
    * Update user profile information
    * Upload a new user picture to the server

* `ReachabilityService` contains methods to work with network reachability status.

* `RecordsService` contains methods related to video recording:
    * Request a paginated list of videos recorded by the current user
    * Request video information by a given raw link
    * Request the creation of a new upload
    * Request to retry an upload
    * Mark upload as done
    * Request a list of uploaded videos
    * Cancel uploading by a given identifier
    * Request upload progress status by a given identifier

* `SettingsService` contains methods related to the global settings of the VeriFlix Media Platform:
    * Request information about maximum video recording length
    * Update device geolocation information

* `StateService` contains methods related to the local user state and settings:
    * Is user currently logged in
    * Current user profile
    * Should videos

* `StorageService` contains methods related to working with internal storage:
    * Get files from Saved storage
    * Get Saved storage size
    * Delete files from Saved storage
    * Delete Saved storage itself

* `DraftVideosService` containts methods to work with draft videos:
    * Save draft video information to storage
    * Get draft video information
    * Delete information about one video
    * Delete the whole storage

* `MergeDraftsService` was implemented in collaboration with `DraftVideosService` since there was a problem with merging parts of the video, so it containts methods:
    * Merge videos
    * Save current merge location
    * Delete previous merged video
    * Find merged video

* `OrientationService` contains methods to get the information about video orientation and use it in the future:
    * Save information
    * Get information
    * Clear it

## UI components

* `VeriflixRecorderViewController` encapsulates the whole recording flow which consists of the following screens:
    * Camera recording screen
    * Edit (cut) video screen
    * Preview video/photo screen
    * Add description to video/photo screen
    * Uploading screen
    * Upload complete/failed screen

## Suggested workflow of using SDK

1. Configure the library
1. Check if SDK services are available
1. Authenticate user (if user is not authenticated yet)
1. Check if all needed permissions are granted to the application and request them if necessary
1. _Optional:_ Update information about current device geolocation
1. Use `VeriflixRecorderViewController` to record and upload a video or photo.
1. _Optional:_ Request a list of videos and if some of the failed to upload, retry uploading.
<!-- 1. If app supports Missions:
    1. Request a list of nearby missions
    1. Wait for push messages
    1. Use `VeriflixRecorderViewController` to record a video with mission -->

### Configuration
In your AppDelegate add `import VeriflixSDK` at the top and use `Veriflix.configure` in `didFinishLaunchingWithOptions` to setup SDK:

``` {.c}
import VeriflixSDK

func application (
    _ application: UIApplication,
     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    Veriflix.configure(
        config: .init(
            consumerId: "yourConsumerId",
            environment: .qa // Use .prod for production server
        )
    )

    return true
}
```    

### Check SDK availability
It is recommended to check if SDK/backend services are available at this moment. You can do it by using `AvailabilityService`.

Example:

```
Veriflix.sdk.availabilityService.isSdkAvailable { available in
    guard available else {
        // Show message to the user to try later.
        return
    }

    // Continue with login/recording/etc
}
```

### Authentication

Authentication is required for most actions using the library. Methods from the `AuthService` class can be used for this purpose:

* `isAuthenticated` - Returns a bool value, true after successful authentication and before log out, false otherwise
* `login` - Performs authentication of an existing user and returns and authentication token
* `signUp` - Performs authentication of a new user and returns an authentication token
* `resetPassword` - Sends an email to the supplied email address of an existing user with a link to reset current password
* `checkAvailability` - Check the availability of a new username

Authentication can be achieved by login for an existing user or sign up for a new user.

The need for authentication can be determined by the result of the `isAuthenticated` property from `AuthenticationService` class. However, please note that this method can return a false positive result if, for some reason, an authentication token is invalid. Therefore if is necessary to always provide a handling of failure result for requests requiring prior authentication, and handle the `.unauthenticated` error during re-authentication.

Deactivation of an authenticated session on the server can be performed by calling the `logout` method from `AuthenticationService` class.

Registration example:

``` {.c}
Veriflix.sdk.authService.signUp(email: email, firstName: firstName, lastName: lastName) { result in
    switch result {
    case .success(let authToken):
        // Handle auth token
    case .failure(let error):
        // Handle error
        break
    }
}
```

### Update information about current device geolocation

The `updateCurrentPosition` method of the `SettingsService` class accepts longitude and latitude arguments of type `Double`. This method updates the user's current location on the server. Is should be called each time a new location update has been received by your app.

Update position example:

``` {.c}
func updatePosition(
    latitude: Double,
    longitude: Double,
    completion: ((_ successfull: Bool) -> Void)?
) {       
    Veriflix.sdk.settingsService.updateCurrentPosition(
        longitude: longitude,
        latitude: latitude
    ) { result in
        switch result {
        case .success:
            completion?(true)
        case .failure(let error):
            completion?(false)
        }
    }
}
```

### Video recording and sending

#### Example

Here is example how to perform recording of photo or video with comments.  
Make sure to add `NSCameraUsageDescription`/`NSMicrophoneUsageDescription`/`NSPhotoLibraryUsageDescription` keys with correspponding values suitable to your app to Info.plist as described in Apple documentation.

``` {.c}
func openRecorder() {
    // Ensure the user is logged in:
    guard Veriflix.sdk.authService.isAuthenticated else {
        return
    }
    
    // Ensure that we have needed permissions:
    Veriflix.sdk.permissionsService.checkOrRequest(permissions: [.camera, .microphone]) { allGranted in
        guard allGranted else {
            return
        }

        // Create configuration object:
        let config = RecorderConfiguration(
            mode: video,
            source: .camera,
            includeDescriptionField: false,
            theme: theme,
            location: currentLocation,
            editorSettings: [],
            hasAutoFill: false,
            supportsPortraitMode: false,
            isEditingPortrait: false
        )

        // Create output object which is used to receive callbacks from recorder
        let output = RecorderOutput()

        // Create recorder controller itself
        let vc = VeriflixRecorderViewController(configuration: config, output: output)

        output.done = {
            vc.dismiss(animated: true)
        }

        vc.modalPresentationStyle = .fullScreen

        // And show it to the user
        self.present(vc, animated: true)
    }
}
```

#### Additional configuration
You may pass additional parameters to `RecorderConfiguration` object. For example:  

* `source` may be of type `.library`, when you provide video/photo from Photo Library:
``` {.c}
public enum RecorderSource {
    case camera
    case library(assets: [RecorderLibraryAsset])
}
```
* Use `theme` parameter to customize appearance and colors on on screens during recording flow (see [Theme Support](#theme-support) section)
<!-- * Use `mission` parameter if video recording is initiated by mission push -->

### Retrying upload

If an upload has been marked as failed for any reason, it is possible to retry to upload.

The `retryRecordUpload(id:)` method from the `RecordsService` class will schedule an upload retry for an upload specified by a given identifier.

``` {.c}
Veriflix.sdk.recordsService.retryRecordUpload(id: uploadId)
```

### Recorded videos

The `requestRecords` method from the `RecordService` returns the first page of a list of recorded videos already sent to the server. The result of this method also contains a link to the next page of results. Use `query` = nil to get all recorded videos without filtering.

``` {.c}
Veriflix.sdk.recordsService.requestRecords(query: "searchText") { result in
    switch result {
    case .success(let page):
        let records = page.results
        self?.showRecordsList(records: records)
        break
    case .failure(let error):
        // Handle error
        break
    }
}
```

The `requestRecords(link:)` method works similar to the `requestRecords` method but accepts a page link as the argument.

``` {.c}
Veriflix.sdk.recordsService.requestRecords(link: nextPageLink) { [weak self] result in
    switch result {
    case .success(let page):
        let records = page.results
        self?.showRecordsList(records: records)
        break
    case .failure(let error):
        // Handle error
        break
    }
}
```

The `requestRecord(id:)` method returns specific recorded video by id.

``` {.c}
Veriflix.sdk.recordsService.requestRecord(id: "recordId") { [weak self] in
    switch result {
    case .success(let record):
        self?.showRecordInfo(record: record)
        break
    case .failure(let error):
        // Handle error
        break
    }
}
```

### User Profile

#### Getting a user profile information

The `requestUser` method in the `ProfileService` class returns a `User` object with user profile information, including optional fields.

``` {.c}
Veriflix.sdk.profileService.requestUser { [weak self] result in
    switch result {
    case .success(let user):
        self?.showUserInfo(user: user)
        break
    case .failure(let error):
        // Handle error
        break
    }
}
```

#### Updating a user profile information

The `updateUser` method in the `ProfileService` class accepts a `User` object to make changes to the user profile.

``` {.c}
Veriflix.sdk.profileService.updateUser(user: user) { [weak self] result in
    switch result {
    case .success(let user):
        break
    case .failure(let error):
        // Handle error
        break
    }
}
```

## Theme support
You can pass `VeriflixTheme` object to UI components to customize colors/images/settings.

- Here is description of color fields:
    - `text`:
    Text color.
    - `highlight`:
    Used for buttons without background, hashtag color, upload control's progress color.
    - `buttonBackground`:
    Used for buttons with background
    - `screenBg`:
    Screen background color
    - `inputFieldText`:
    Text color for input fields.
    - `inputFieldBg`:
    Background color for input fields.
    - `inputFieldBorder`:
    Border color for input fields.
    - `inputFieldInactiveText`:
    Text color for inactive input fields.
    - `inputFieldInactiveBg`:
    Background color for inactive input fields.
    - `inputFieldInactiveBorder`:
    Border color for inactive input fields.
    - `radioButtonNotSelectedBg`:
    Background color for not selected radio buttons.
    - `radioButtonNotSelectedBorder`:
    Border color for not selected radio buttons.
    - `radioButtonNotSelectedCircleBg`:
    Background color of radio button knob for not selected radio buttons.
    - `radioButtonNotSelectedCircleBorder`:
    Border color of radio button knob for not selected radio buttons.
    - `radioButtonNotSelectedText`:
    Text color for not selected radio buttons.
    - `radioButtonSelectedBg`:
    Background color for selected radio buttons.
    - `radioButtonSelectedBorder`:
    Border color for selected radio buttons.
    - `radioButtonSelectedCircleBg`:
    Background color of radio button knob for selected radio buttons.
    - `radioButtonSelectedCircleBorder`:
    Border color of radio button knob for selected radio buttons.
    - `radioButtonSelectedText`:
    Text color for selected radio buttons.
    - `uploaderBg`:
    Background color for upload indicator on upload screen.
    - `uploaderProgressBg`:
    Background color for progress part for upload indicator on upload screen.
    - `uploaderProgress`:
    Color of progress part for upload indicator on upload screen.
    - `uploaderProgressFailed`:
    Color of progress part for upload indicator on upload screen when upload fails.
    - `uploaderProgressWaiting`:
    Color of progress part for upload indicator on upload screen when upload is pause because of missing network.

- Here is description of image fields:

    - `logo`:  
    Image which will be used on Add comment and Upload screens.

- Here is description of settings fields:

    - `showLogoOnAddCommentScreen`:  
    Indicates whether logo should be shown on Add comment screen.

- Here is description of fonts fields:

    - `font`:  
    Font which will be used on Recorder screens. Only font name is used from provided font reference (not font size).

### SI Theme

There's a static function in `VeriflixTheme` class that provides colors and logo for SI design. It uses default font but there's an option to pass custom font to theme initializator. Usage example:

``` {.c}
let theme = VeriflixTheme.SITheme()

let theme = VeriflixTheme.SITheme(fonts: UIFont(name: "customFont"))
```

## Error types

Methods in VeriFlix SDK may return a number of errors:

* `.invalidResponse(let info)` - returned when response code is not acceptable, provides HTTP status code and dictionary of errors

* `.validationError(let errors)` - returned when posted data is invalid, provides array of error

* `.unauthorized` - returned when the method called requires authentication but the stored authentication token is empty or not valid.

* `.unknownError(let description)` - returned in other cases with description

# Changelog

## [1.2.0] - 2021-11-03
### Added
- Added `VeriflixAvailabilityService` which allows to check for availability of SDK/backend services before trying to record video. See [Check SDK availability](#check-sdk-availability) part.
- Added `Fonts` struct to `VeriflixTheme` which allows to specify font which will be used on recorder screens.
- Add third category "Magazine" on Add Comment screen (non-LNG consumers)

### Changes
- Hide "Title and Infos" title on Add screen for LNG consumer

### Fixed
- Translations of fields on Add Comment screen
- Font sizes on Add Comment

## [1.3.0] - 2021-26-03
### Added
- Added `VeriflixStorageService` which allows to work with internal storage used for new re-edit feature. See [Working with internal storage](#working-with-internal-storage).

### Changes
- Added `isUploadingPhoto` flag to `AddCommentViewController` in order to indicate if photo is being uploaded and hide `autoValidateSwifch`.
- Changed `congratsSubtitlePhoto` localizable string.

### Fixed
- Fixed `currentConsumer` variable in `ConsumerService` since it was always `nil`.
- Called `setup()` function in `WrongOrientationView` since it was removed in some previous changes.

### [1.6.0] - 2022-20-09

### Added
- Added `VeriflixDrafrVideosService` which allows to work with draft videos
- Added `VeriflixMergeDraftsService` to merge draft videos while filming
- Added `VeriflixOrientationService` to check current filming video orientation and forbid to shoot in different orientations