#!/bin/bash -ex

program_exists ()
{
    type "$1" &> /dev/null ;
}

main()
{
    set -e
    pushd $(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd) >/dev/null

    if ! program_exists markdown-toc; then
        echo "Please install markdown-toc (https://github.com/jonschlinkert/markdown-toc): npm install --global markdown-toc"
        exit 1
    fi

    if ! program_exists pandoc; then
        echo "Please install pandoc (https://pandoc.org/installing.html): brew install pandoc"
        exit 1
    fi

    markdown-toc -i ../index.md 

    echo "<style>" >> tmp.css
    cat github-markdown.css >> tmp.css
    echo "</style>" >> tmp.css
    
    pandoc -s --highlight-style pygments -H tmp.css --metadata title='VeriFlix iOS SDK documentation' ../index.md > ../index.html

    rm tmp.css

    popd >/dev/null
}

main "$@"
